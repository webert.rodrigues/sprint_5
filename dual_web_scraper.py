import requests
from bs4 import BeautifulSoup
import pandas as pd

def scrap_mercadolivre():
    url = 'https://www.mercadolivre.com.br/ofertas?container_id=MLB779362-4&category=MLB1055&promotion_type=deal_of_the_day#deal_print_id=937c4e00-8eeb-11ee-a0da-b74879879683&c_id=carouseldynamic-normal&c_element_order=undefined&c_campaign=VER-MAIS&c_uid=937c4e00-8eeb-11ee-a0da-b74879879683'
    url_indesejada = ['https://www.mercadolivre.com.br/ofertas?promotion_type=deal_of_the_day&container_id=MLB779362-4&category=MLB1055&page=']
    list_url_produtos = []

    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')
    produtos = soup.find('div', class_='promotions_boxed-width')
    links = produtos.find_all('a')

    dados_produtos = []
    
    for link in links:
        for url in url_indesejada:
            if url not in link['href']:
                list_url_produtos.append(link['href'])


    for url in list_url_produtos:
        response = requests.get(url)
        soup = BeautifulSoup(response.text, 'html.parser')

        nome = soup.find('h1', class_='ui-pdp-title').text.strip()
        preco = soup.find('span', class_='andes-money-amount__fraction').text.strip()

        dados_produtos.append({'Nome': nome, 'Preço': preco})

    df = pd.DataFrame(dados_produtos)

    df.to_csv('celulares.csv', index=False)

def scrap_books_to_scrape():
    url = 'https://books.toscrape.com/'
    list_url_produtos = []
    dados_produtos = []

    for i in range(5):
        response = requests.get(f'{url}/catalogue/page-{i+1}.html')
        soup = BeautifulSoup(response.text, 'html.parser')
        produtos = soup.find('ol')
        links = produtos.find_all('a', attrs={'title': True})

        for link in links:
            if link['href']:
                list_url_produtos.append(link['href'])

    for url_produto in list_url_produtos:
        response = requests.get(f'{url}/catalogue/{url_produto}')
        soup = BeautifulSoup(response.text, 'html.parser')

        nome = soup.find('h1').text.strip()
        preco = soup.find('p', class_='price_color').text.strip().replace('Â£', '')

        if preco and nome:
            preco = float(preco)
            nome.replace('"', '')
            dados_produtos.append({'Nome': nome, 'Preço em Euros': preco})

    df = pd.DataFrame(dados_produtos)

    df.to_csv('livros.csv', index=False)

if __name__ == "__main__":
    scrap_mercadolivre()
    scrap_books_to_scrape()

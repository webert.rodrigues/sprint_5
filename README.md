# TerraLAB

## Atividade Prática - Sprint 5

##### por Webert Meira Rodrigues

## SPRINT 5

> **Descrição:** 
> O projeto Dual Scraper consiste em um script em Python para realizar web scraping em dois sites distintos: Mercado Livre e Books to Scrape. O objetivo é extrair informações sobre produtos desses sites, incluindo nome e preço.
<hr>

> **Funcionalidades Principais:** 
>
> O Dual Scraper permite extrair informações de produtos dos sites Mercado Livre e Books to Scrape simultaneamente. O script organiza os dados coletados e os salva em arquivos CSV, facilitando a análise posterior.
<hr>

> **Tecnologias Utilizadas:** 
>
> *Python, Requests, BeautifulSoup, Pandas*
<hr>

> **Como Usar:** 
>  Antes de executar os comandos abaixo, certifique-se de ter o Python instalado na sua máquina.
> ```js
>  # Inicie a ambiente virtual (caso queira)
>  python -m venv venv
>
>  # Ative o ambiente virtual
>  .\venv\Scripts\activate
>
>  # Instale as dependências
>  pip install -r requirements.txt
>
>  # Execute o script para realizar o scraping em ambos os sites
>  python dual_scraper.py
> ``` 
>  O script gerará dois arquivos CSV: celulares.csv com informações do Mercado Livre e livros.csv com dados do Books to Scrape.
<hr>